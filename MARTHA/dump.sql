CREATE TABLE items (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

INSERT INTO items(name) VALUES ("aaa");
INSERT INTO items(name) VALUES ("bbb");
INSERT INTO items(name) VALUES ("ccc");