package com.example.marthahttpdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);

        findViewById(R.id.buttonAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject params = new JSONObject();
                try {
                    // Construire un objet JSON contenant les données
                    params.put("name", "item" + System.currentTimeMillis());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Le second paramètres est l'object JSON
                MarthaRequest insertRequest = new MarthaRequest("insert-item", params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(MainActivity.this, "INSERTED ID="+response.getInt("lastInsertId"), Toast.LENGTH_SHORT).show();

                            refresh(); // Mettre a jour la liste
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, null); // TODO: Gerer errorListener pour un meilleur UX

                MarthaQueue.getInstance(MainActivity.this).send(insertRequest);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        refresh();
    }

    private void refresh() {
        MarthaRequest request = new MarthaRequest("select-items", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        text.setText(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("DEMO", "onErrorResponse: " + error.toString());
                    }
                }
        );

        MarthaQueue.getInstance(this).send(request);
    }
}